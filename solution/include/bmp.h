//
// Created by 1 on 28.12.2021.
//

#pragma once

#include "inner_format.h"
#include "stdbool.h"
#include "stdint.h"


#pragma pack(push,1)

struct __attribute__((packed)) bmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct bmpImage {
    struct bmpHeader header;
    struct pixel* pixels;
};

struct bmpImage readBmp(const char* path);

struct image bmpToImage(struct bmpImage bmp);

struct bmpImage imageToBmp(struct image img);

void writeBmp(const char* path, struct bmpImage bmp);

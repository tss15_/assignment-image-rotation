//
// Created by 1 on 27.12.2021.
//

#pragma once

#include "stdint.h"
#include "stdlib.h"

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image createImage(uint64_t width, uint64_t height, struct pixel* pixels);

void freeImage(struct image img);

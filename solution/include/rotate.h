//
// Created by 1 on 28.12.2021.
//

#pragma once

#include "inner_format.h"

struct image rotateCW(struct image source);
struct image rotateACW(struct image source);
struct image horizontalMirror(struct image source);
struct image verticalMirror(struct image const source);

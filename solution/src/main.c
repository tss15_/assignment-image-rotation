#include "../include/bmp.h"
#include "../include/rotate.h"
#include "stdio.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Incorrect arguments count, expected 2 (input file path and output file path)");
        return 1;
    }

    struct bmpImage sourceBmp = readBmp(argv[1]);

    struct image sourceImage = bmpToImage(sourceBmp);
    struct image transformedImage = rotateACW(sourceImage);

    freeImage(sourceImage);

    struct bmpImage transformedBmp = imageToBmp(transformedImage);
    writeBmp(argv[2], transformedBmp);

    freeImage(transformedImage);
    fprintf(stdout, "Success!");
    return 0;
}

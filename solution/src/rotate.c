//
// Created by 1 on 28.12.2021.
//

#include "../include/rotate.h"

///rotate anti-clockwise
struct image rotateACW(struct image const source) {
    struct image res = createImage(source.height, source.width, NULL);

    uint64_t rows = res.height;
    uint64_t cols = res.width;

    for (uint64_t r = 0; r < rows; r++) {
        for (uint64_t c = 0; c < cols; c++) {
            res.data[r * cols + c] = source.data[rows * (cols - c - 1) + r];
        }
    }
    return res;
}

struct image horizontalMirror(struct image const source) {
    struct image res = createImage(source.width, source.height, NULL);

    uint64_t rows = res.height;
    uint64_t cols = res.width;

    for (uint64_t r = 0; r < rows; r++) {
        for (uint64_t c = 0; c < cols; c++) {
            res.data[r * cols + c] = source.data[(rows - r - 1) * cols + c];
        }
    }
    return res;
}

struct image verticalMirror(struct image const source) {
    struct image res = createImage(source.width, source.height, NULL);

    uint64_t rows = res.height;
    uint64_t cols = res.width;

    for (uint64_t r = 0; r < rows; r++) {
        for (uint64_t c = 0; c < cols; c++) {
            res.data[r * cols + c] = source.data[cols * r + (cols - c - 1)];
        }
    }
    return res;
}

///BONUS: rotate clockwise using anti-clockwise rotation and mirroring
struct image rotateCW(struct image const source) {
    return verticalMirror(horizontalMirror(rotateACW(source)));
}

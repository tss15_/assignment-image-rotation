//
// Created by 1 on 28.12.2021.
//

#include "../include/bmp.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"

 const uint16_t format = 19778;
    const uint32_t info_header_size = 40;
    const uint16_t planes = 1;
    const uint16_t bit_count = 24;

///read_bmp: reads header first then the data itself
///header contains info about image size
///data contains raw pixel values
///foresees potential errors
struct bmpImage readBmp(const char *path) {
    struct bmpImage bmp = {0};
    FILE *fp = fopen(path, "rb");

    if (fp) {
        if (fread(&bmp.header, sizeof(struct bmpHeader), 1, fp)) {
            if (bmp.header.bfType == format) {

                bmp.pixels = malloc(bmp.header.biWidth * bmp.header.biHeight * sizeof(struct pixel));

                const uint8_t padding = bmp.header.biWidth % 4;
                size_t row_bytes;

                bool isSuccessful = true;

                size_t row = 0;
                while (isSuccessful && row < bmp.header.biHeight) {
                    row_bytes = fread(&(bmp.pixels[(row++) * bmp.header.biWidth]),
                                      sizeof(struct pixel), bmp.header.biWidth, fp);
                    int fseekResult = 0;                  
                    fseekResult = fseek(fp, padding, SEEK_CUR);
                    isSuccessful = (bmp.header.biWidth == row_bytes && fseekResult == 0);
                }

                if (isSuccessful) {
                    fclose(fp);
                    return bmp;
                }

                fprintf(stderr, "Problems while reading image data");
                fclose(fp);
                exit(1);

            } else {
                fprintf(stderr, "Incorrect header");
                fclose(fp);
                exit(1);
            }
        } else {
            fprintf(stderr, "Problems while reading header");
            fclose(fp);
            exit(1);
        }
    } else {
        fprintf(stderr, "No such file");
        exit(1);
    }
}

///create image using constructor
struct image bmpToImage(struct bmpImage bmp) {
    return createImage(bmp.header.biWidth, bmp.header.biHeight, bmp.pixels);
}

///create bmp header with default values and image parameters
struct bmpHeader createBmpHeader(struct image img) {
    uint32_t bOffBits = sizeof(struct bmpHeader);
    uint32_t biSizeImage = img.width * img.height * sizeof(struct pixel) + (img.width % 4) * img.height;

    struct bmpHeader header = {
            format,
            bOffBits + biSizeImage,
            0,
            bOffBits,
            info_header_size,
            img.width,
            img.height,
            planes,
            bit_count,
            0,
            biSizeImage,
            0,
            0,
            0,
            0
    };

    return header;
}

///convert image to bmp: write header first then image data
struct bmpImage imageToBmp(struct image img) {
    struct bmpImage bmp = {
            .header=createBmpHeader(img),
            .pixels=img.data
    };
    return bmp;
}

///write_bmp: writes header first then the data itself
///header contains info about image size
///data contains raw pixel values
///foresees potential errors
void writeBmp(const char *path, struct bmpImage bmp) {
    FILE *fp = fopen(path, "wb");

    if (fp != NULL) {

        if (fwrite(&bmp.header, sizeof(struct bmpHeader), 1, fp)) {

            uint8_t padding = bmp.header.biWidth % 4;
            size_t row_bytes;
            const uint64_t zero = 0;

            bool isSuccessful = true;
            size_t row = 0;

            while (isSuccessful && row < bmp.header.biHeight) {
                row_bytes = fwrite(&(bmp.pixels[(row++) * bmp.header.biWidth]),
                                   sizeof(struct pixel), bmp.header.biWidth, fp);
                fwrite(&zero, 1, padding, fp);
                isSuccessful = (bmp.header.biWidth == row_bytes);
            }
            if (isSuccessful) {
                fclose(fp);
            } else {
                fprintf(stderr, "Problems while writing image data");
                fclose(fp);
                exit(1);
            }

        } else {
            fprintf(stderr, "Problems while writing header");
            fclose(fp);
            exit(1);
        }
    } else {
        fprintf(stderr, "Cannot open output file");
        exit(1);
    }
}


//
// Created by 1 on 27.12.2021.
//

#include "../include/inner_format.h"
#include "stdint.h"
#include "stdlib.h"

///image contains parameters width, height and data of pixel values
struct image createImage(uint64_t width, uint64_t height, struct pixel* pixels) {
    struct image img = {
            .width = width,
            .height = height,
    };
   if (pixels == NULL)
        img.data = malloc(sizeof(struct pixel) * width * height);
   else img.data = pixels;
    return img;
}

void freeImage(struct image img) {
    free(img.data);
}

